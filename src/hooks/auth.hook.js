import { useState, useCallback, useEffect } from 'react'
import axios from 'axios'

const storageName = 'userData'

export const useAuth = () => {
    const [token, setToken] = useState(null)
    const [userId, setUserId] = useState(null)

    const login = useCallback((jwtToken, id) => {
        setToken(jwtToken)
        setUserId(id)

        localStorage.setItem(storageName, JSON.stringify({
            userId: id,
            token: jwtToken
        }))
    }, [])

    const logout = useCallback(() => {
        setToken(null)
        setUserId(null)
        localStorage.removeItem(storageName)
    }, [])

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem(storageName))

        //Добавить проверку токена на валидность

        if (data && data.token) {

            axios.get('/user/profile', { headers: { 'token': data.token } })
                .then(res => {
                    login(data.token, data.userId)
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }, [login])


    return { login, logout, token, userId }
}

import React from 'react'
import { useRoutes } from './routes'
import { BrowserRouter as Router} from 'react-router-dom'
import axios from 'axios'
import config from './config.json'
import { useAuth } from './hooks/auth.hook'
import { AuthContext } from './context/AuthContext'
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './theme';


axios.defaults.baseURL = config.serverURL

function App() {

  const {token, login, logout,  userId } = useAuth()
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated)

  // console.log(isAuthenticated)

  return (
    <AuthContext.Provider value={{
      token, login, logout,  userId, isAuthenticated
    }}>
      <ThemeProvider theme={theme}>
        <Router>
          {routes}
        </Router>
      </ThemeProvider>
    </AuthContext.Provider>
  );
}

export default App;

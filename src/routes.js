import React from 'react'
import { Switch, Route } from 'react-router-dom'
import UserPage from './pages/UserPage'
import MainPage from './pages/MainPage'
import SigninPage from './pages/SigninPage'
import SignupPage from './pages/SignupPage'
import Dashboard from './components/dashboard'
import AddPhotoPage from './pages/AddPhotoPage'
import AddAlbumPage from './pages/AddAlbumPage'
import AlbumsPage from './pages/AlbumsPage'

export const useRoutes = isAuthenticated => {

    if (isAuthenticated) {
        return (

            <Switch>
                <Route path="/" exact>
                    <MainPage />
                </Route>
                <Route path="/addPhoto">
                    <Dashboard>
                        <AddPhotoPage />
                    </Dashboard>
                </Route>
                <Route path="/addAlbum">
                    <Dashboard>
                        <AddAlbumPage />
                    </Dashboard>
                </Route>
                <Route path="/user">
                    <Dashboard>
                        <UserPage />
                    </Dashboard>
                </Route>
                <Route path="/albums">
                    <Dashboard>
                        <AlbumsPage />
                    </Dashboard>
                </Route>
                {/* <Redirect to="/user" /> */}
            </Switch>

        )
    }

    return (
        <Switch>
            <Route path="/signin">
                <SigninPage />
            </Route>
            <Route path="/signup">
                <SignupPage />
            </Route>
            <Route path="/" exact>
                <MainPage />
            </Route>
            {/* <Redirect to="/" /> */}
        </Switch>
    )
}
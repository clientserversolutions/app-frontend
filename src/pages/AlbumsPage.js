import React, { useContext, useEffect, useState } from 'react';
// import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { AuthContext } from '../context/AuthContext'
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'
import Avatar from '@material-ui/core/Avatar'
import PhotoAlbumIcon from '@material-ui/icons/PhotoAlbum';
import ButtonBase from '@material-ui/core/ButtonBase';
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'



const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(8)
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',

    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    avatar2: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.primary.main,
    },
    paper2: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        marginTop: '50px'
    },
}));

function AlbumPage(props) {

    const auth = useContext(AuthContext)

    const [albumsInfo, setAlbumsInfo] = useState([])
    const [requestData, setRequestData] = useState('')
    const [albumIdField, setAlbumIdField] = useState('')

    const classes = useStyles();

    useEffect(() => {
        axios.get(`/album`, { headers: { 'token': auth.token } })
            .then(function (response) {
                setAlbumsInfo(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
    }, [auth.token, requestData])

    const deleteAlbum = (albumId) => {
        axios.delete(`/album`, { data: { albumId }, headers: { 'token': auth.token } })
            .then(function (response) {
                console.log(response);
                setRequestData(new Date())
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    const addAlbum = (albumId) => {
        axios.post(`/album/${albumId}`,{}, {
            headers: { 'token': auth.token }
        })
            .then(function (res) {
                console.log(res);
                props.history.push('/user')
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    const onAlbumIdInput = (event) => {
        setAlbumIdField(event.target.value)
    }

    const copyLink = (albumId) => {
        navigator.clipboard.writeText(albumId)
            .then(() => {
                // Получилось!
            })
            .catch(err => {
                console.log('Something went wrong', err);
            });
    }

    return (
        <>
            <Container className={classes.cardGrid} maxWidth="lg">
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <PhotoAlbumIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Мои альбомы
                    </Typography>
                </div>
                <Paper className={classes.paper2}>
                    <Grid container spacing={4}>
                        <Grid item lg={12}>
                            <Typography component="h1" variant="h5">
                                Добавить альбом по идентификатору
                            </Typography>
                        </Grid>
                        <Grid item lg={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="albumId"
                                label="Идентификатор альбома"
                                name="albumId"
                                onChange={onAlbumIdInput}
                            />
                        </Grid>
                        <Grid item lg={12} >
                            {/* <div className={classes.buttonBlock}> */}
                            <Button variant="contained" color="primary" size="large" onClick={() => addAlbum(albumIdField)}>
                                Добавить
                            </Button>
                            {/* </div> */}
                        </Grid>
                    </Grid>
                </Paper>

                <br />
                <br />


                <Grid container spacing={4}>
                    {albumsInfo.map((item, index) => (
                        <Grid item key={index} xs={12} sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardActions>
                                    <Button size="small" color="primary" onClick={() => copyLink(item.id_album)}>
                                        Скопировать идентификатор
                                    </Button>
                                </CardActions>
                                <ButtonBase style={{ backgroundColor: '#eee' }} >
                                    <div style={{ textAlign: 'center', height: '200px' }}>
                                        <Avatar className={classes.avatar2} >
                                            <PhotoAlbumIcon fontSize={'large'} />
                                        </Avatar>
                                    </div>
                                    <CardContent className={classes.cardContent}>
                                        <Typography gutterBottom variant="h6" component="h2">
                                            {item.album_name}
                                        </Typography>
                                        <Typography>
                                            {item.date_created}
                                        </Typography>
                                    </CardContent>

                                </ButtonBase>
                                <CardActions>
                                    <Button size="small" color="primary" onClick={() => deleteAlbum(item.id_album)}>
                                        Удалить
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>


            </Container>
        </>
    );
}

// UserPage.propTypes = {
//     loading: PropTypes.bool,
// };

export default function YouTube() {
    return (
        <Box overflow="hidden">
            <AlbumPage />
        </Box>
    );
}
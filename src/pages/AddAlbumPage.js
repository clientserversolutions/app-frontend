import React, { useContext, useState } from 'react'
import { DropzoneArea } from 'material-ui-dropzone'
import Button from '@material-ui/core/Button';
import axios from 'axios'
import { AuthContext } from '../context/AuthContext'
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar'
import AddToPhotosIcon from '@material-ui/icons/AddToPhotos';
import { withRouter } from 'react-router-dom';
import Paper from '@material-ui/core/Paper'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const useStyles = makeStyles((theme) => ({

    cardGrid: {
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(2),
    },
    paper: {
        marginTop: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    buttonBlock: {
        textAlign: 'right'
    },
    paper2: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,

    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',

    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
}));


function AddAlbumPage(props) {
    const classes = useStyles();
    const auth = useContext(AuthContext)

    const [albumName, setAlbumName] = useState('')
    const [photo, setPhoto] = useState('')
    const [video, setVideo] = useState('')
    const [photos, setPhotos] = useState([])
    const [videos, setVideos] = useState([])
    const [pairName, setPairName] = useState('')
    const [pairNames, setPairNames] = useState([])
    const [pairs, setPairs] = useState([])

    const [refresh, setRefresh] = useState(0)



    const onAlbumNameInput = (event) => {
        setAlbumName(event.target.value)
    }

    const onPairNameInput = (event) => {
        setPairName(event.target.value)
    }
    const handleAddPhoto = (files) => {
        setPhoto(files[0])

        // if (files.length)
        //     setPhoto(photo => [...photo, files[0]])
    }
    const handleAddVideo = (files) => {
        setVideo(files[0])

        // if (files.length)
        //     setVideo(video => [...video, files[0]])
    }
    const addPair = () => {
        if (photo && video && pairName) {
            setPhotos(photos => [...photos, photo])
            setVideos(videos => [...videos, video])
            setPairNames(pairNames => [...pairNames, pairName])
            setPairs(pairs => [...pairs, {
                photo: URL.createObjectURL(photo),
                video,
                pairName

            }])
        }

        setPhoto('')
        setVideo('')
        setPairName('')
        setRefresh(refresh + 1)
    }

    const sendPair = () => {

        let formData = new FormData();

        photos.forEach(element => {
            formData.append('photos', element)
        });

        videos.forEach(element => {
            formData.append('videos', element)
        });

        formData.set('albumName', albumName)
        formData.set('pairNames', pairNames)

        axios.post('/album', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'token': auth.token
            }
        })
            .then(function (res) {
                console.log(res);
                props.history.push('/albums')
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    return (
        <>
            <Container className={classes.cardGrid} maxWidth="lg">
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <AddToPhotosIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Создание альбома живых фотографий
                    </Typography>
                    <br />

                    <br />
                    <Paper className={classes.paper2}>

                        <br />
                        <br />
                        <Grid container spacing={4}>
                            <Grid item lg>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="name"
                                    label="Название фотографии"
                                    name="name"
                                    autoFocus
                                    onChange={onPairNameInput}
                                    value={pairName}
                                />
                            </Grid>
                            <Grid item lg>
                                <DropzoneArea
                                    key={refresh}
                                    maxFileSize={100000000}
                                    filesLimit={1}
                                    acceptedFiles={['image/*']}
                                    showPreviewsInDropzone={true}
                                    onChange={handleAddPhoto}
                                    dropzoneText={'Перетащите или выберите фотографию'}
                                />
                            </Grid>
                            <Grid item lg>
                                <DropzoneArea
                                    key={refresh}
                                    maxFileSize={100000000}
                                    filesLimit={1}
                                    showPreviewsInDropzone={true}
                                    acceptedFiles={['video/*']}
                                    onChange={handleAddVideo}
                                    dropzoneText={'Перетащите или выберите видео'}
                                />
                            </Grid>
                        </Grid>
                        <br />
                        <div className={classes.buttonBlock}>
                            <Button color="primary" size="large" onClick={addPair}>
                                Добавить
                            </Button>
                        </div>
                    </Paper>

                    <br />
                    <br />

                    <Grid container spacing={4}>
                        {pairs.map((card, index) => (
                            <Grid item key={index} xs={12} sm={6} md={4}>
                                <Card className={classes.card}>
                                    <div style={{ textAlign: 'center', height: 200 }}>
                                        <img src={card.photo} style={{ height: 200 }} alt={card.pairName} />
                                    </div>
                                    <CardContent className={classes.cardContent}>
                                        <Typography gutterBottom variant="h6" component="h2">
                                            {card.pairName}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>

                    <br />
                    <br />

                    <Paper className={classes.paper2}>
                        <Grid container spacing={4}>
                            <Grid item lg={12}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="albumName"
                                    label="Название альбома"
                                    name="albumName"
                                    autoFocus
                                    onChange={onAlbumNameInput}
                                />
                            </Grid>
                            <Grid item lg={12} >
                                {/* <div className={classes.buttonBlock}> */}
                                <Button variant="contained" color="primary" size="large" onClick={sendPair}>
                                    Сохранить
                                    </Button>
                                {/* </div> */}
                            </Grid>
                        </Grid>
                    </Paper>
                </div>
            </Container>
        </>
    )
}

export default withRouter(AddAlbumPage)
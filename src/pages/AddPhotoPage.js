import React, { useContext, useState } from 'react'
import { DropzoneArea } from 'material-ui-dropzone'
import Button from '@material-ui/core/Button';
import axios from 'axios'
import { AuthContext } from '../context/AuthContext'
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar'
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({

  cardGrid: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(2),
  },
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  buttonBlock: {
    textAlign: 'right'
  }
}));


function AddPhotoPage(props) {
  const classes = useStyles();
  const auth = useContext(AuthContext)


  const [photo, setPhoto] = useState([])
  const [video, setVideo] = useState([])
  const [pairName, setPairName] = useState([])

  const onPairNameInput = (event) => {
    setPairName(event.target.value)
  }

  const handleAddPhoto = (files) => {
    setPhoto(files)
  }

  const handleAddVideo = (files) => {
    setVideo(files)
  }

  const sendPair = () => {


    let formData = new FormData();

    formData.append('image', photo[0])
    formData.append('video', video[0])
    formData.set('pairName', pairName)

    axios.post('/img', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'token': auth.token
      }
    })
      .then(function (res) {
        console.log(res);
        props.history.push('/user')
      })
      .catch(function (error) {
        console.log(error);
      });
  }


  return (
    <>
      <Container className={classes.cardGrid} maxWidth="lg">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <AddAPhotoIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Создание живой фотографии
          </Typography>
          <br />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Название фотографии"
            name="name"
            autoFocus
            onChange={onPairNameInput}
          />
          <br />
          <Grid container spacing={4}>
            <Grid item lg>
              <DropzoneArea
                maxFileSize={100000000}
                filesLimit={1}
                acceptedFiles={['image/*']}
                showPreviewsInDropzone={true}
                onChange={handleAddPhoto}
                dropzoneText={'Перетащите или выберите фотографию'}
              />
            </Grid>
            <Grid item lg>
              <DropzoneArea
                maxFileSize={100000000}
                filesLimit={1}
                showPreviewsInDropzone={true}
                acceptedFiles={['video/*']}
                onChange={handleAddVideo}
                dropzoneText={'Перетащите или выберите видео'}
              />
            </Grid>
          </Grid>

          <br />


          <br />

        </div>
        <div className={classes.buttonBlock}>
          <Button variant="contained" color="primary" size="large" onClick={sendPair}>
            Сохранить
          </Button>
        </div>
      </Container>
    </>
  )
}

export default withRouter(AddPhotoPage)
import React, { useContext, useEffect, useState } from 'react';
// import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { AuthContext } from '../context/AuthContext'
import axios from 'axios';
import config from '../config.json'

import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'
import Avatar from '@material-ui/core/Avatar'
import ImageIcon from '@material-ui/icons/Image';



const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(8)
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',

    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
}));

function UserPage(props) {
    const auth = useContext(AuthContext)

    const classes = useStyles();

    const [requestData, setRequestData] = useState('')
    const [videosData, setVideosData] = useState([])

    useEffect(() => {
        axios.get('/img', {
            headers: {
                token: auth.token,
                email: auth.userId,
            }
        })
            .then(function (response) {
                console.log(response.data)
                setVideosData(
                    response.data.map(item => ({
                        src: config.serverURL + '/video/stream/' + auth.userId + '/' + item.id_video,
                        name: item.pair_name,
                        date: item.date_created,
                        poster: config.serverURL + '/img/' + item.id_image,
                        id_image: item.id_image
                    })))
            })
            .catch(function (error) {
                console.log(error);
            });

    }, [auth.token, auth.userId, requestData])

    const deletePair = (imageId) => {
        axios.delete(config.serverURL + `/img`, { data: { imageId }, headers: { 'token': auth.token } })
            .then(function (response) {
                console.log(response);
                setVideosData([])
                setRequestData(new Date())
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    const playVideo = (e) => {
        // let src = e.target.children[0].attributes.name.value
        // e.target.src = src
        e.target.play()
    }

    const stopVideo = (e) => {
        e.target.pause()
        // let src = e.target.children[0].attributes.src.value
        // e.target.src = src
        // e.target.src = ''
    }

    return (
        <>
            <Container className={classes.cardGrid} maxWidth="lg">
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <ImageIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Мои живые фотографии
                    </Typography>
                </div>
                <br />
                <br />

                <Grid container spacing={4}>
                    {videosData.map((card, index) => (
                        <Grid item key={index} xs={12} sm={6} md={4}>
                            <Card className={classes.card}>
                                <div style={{ textAlign: 'center', backgroundColor: '#000000', height: 200 }}>
                                    <video style={{ height: 200 }} onMouseEnter={playVideo}
                                        onMouseLeave={stopVideo} poster={card.poster} loop>
                                        {/* onMouseLeave={stopVideo} loop> */}
                                        <source src={card.src} name={card.src} />
                                        {/* <source name={card.src} /> */}
                                    </video>
                                </div>
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="h6" component="h2">
                                        {card.name}
                                    </Typography>
                                    <Typography>
                                        {card.date}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small" color="primary" onClick={() => deletePair(card.id_image)}>
                                        Удалить
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
        </>
    );
}

// UserPage.propTypes = {
//     loading: PropTypes.bool,
// };

export default function YouTube() {
    return (
        <Box overflow="hidden">
            <UserPage />
        </Box>
    );
}